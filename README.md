## Как запустить вручную
Залить код на нужную тачку, далее ввести

`docker build -t <название тэга>:<имя_версии> .`

После сборки 

`docker run -d -e IP_ADDRESS=<interface> -e PORT=<port> -p <port>:<port> <image id>`

Как соберётся, курлыкнуть по следующему адресу

`curl <interface>:<port>/payment`