FROM maven:latest
WORKDIR /app
COPY / ./
CMD mvn clean package

FROM eclipse-temurin:21.0.2_13-jre-ubi9-minimal
COPY --from=0 /app/target/SpringBootService-2.0.jar /app/target/SpringBootService-2.0.jar
CMD java -jar /app/target/SpringBootService-2.0.jar